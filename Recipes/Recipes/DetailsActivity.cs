using System;
using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace Recipes
{
	[Activity(Label = "DetailsActivity")]
	public class DetailsActivity : Android.Support.V7.App.AppCompatActivity
	{
		Recipe recipe;
		ArrayAdapter adapter;
        Android.Support.V7.Widget.Toolbar toolbar;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Details);

			//
			// Retrieve the recipe to be displayed on this page
			//
			int index = Intent.GetIntExtra("RecipeIndex", -1);
			recipe = RecipeData.Recipes[index];

            toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            toolbar.Title = recipe.Name;

            base.SetSupportActionBar(toolbar);

            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_arrow_back_white_24dp);

            //
			// Show the list of ingredients
			//
			var list = FindViewById<ListView>(Resource.Id.ingredientsListView);
			list.Adapter = adapter = new ArrayAdapter<Ingredient>(this, Android.Resource.Layout.SimpleListItem1, recipe.Ingredients);

		}

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            base.MenuInflater.Inflate(Resource.Menu.actions, menu);
            SetFavoriteDrawable(recipe.IsFavorite);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {

            switch (item.ItemId)
            {
                case Resource.Id.addToFavorites:
                    recipe.IsFavorite = !recipe.IsFavorite; // update the recipe's state
                    SetFavoriteDrawable(recipe.IsFavorite); // toggle the image used on the button
                    break;

                case Resource.Id.oneServing: SetServings(1); item.SetChecked(true); break;
                case Resource.Id.twoServings: SetServings(2); item.SetChecked(true); break;
                case Resource.Id.fourServings: SetServings(4); item.SetChecked(true); break;

                case Resource.Id.about:
                    StartActivity(typeof(AboutActivity));
                    break;

                case Android.Resource.Id.Home:
                    Finish();
                    break;
            }

            return true;
        }


        void SetFavoriteDrawable(bool isFavorite)
        {
            if (isFavorite)
            {
                toolbar.Menu.FindItem(Resource.Id.addToFavorites).SetIcon(Resource.Drawable.ic_favorite_white_24dp);
            }
            else
            {
                toolbar.Menu.FindItem(Resource.Id.addToFavorites).SetIcon(Resource.Drawable.ic_favorite_border_white_24dp);
            }
        }


        void SetServings(int numServings)
		{
			recipe.NumServings = numServings;

			adapter.NotifyDataSetChanged();
		}
	}
}